-- SUMMARY --

sclib_enkoder exposes two bits of functionality - a field formatter and a php
function.  Both are for the intended purpose of enkode-ing (per
http://hivelogic.com/enkoder) arbitrary bits of sensitive text, but especially
email addresses, to prevent spambots or other malicious scripts from trivially
nabbing them from the text.

For a full description of the module, see the help page.

-- REQUIREMENTS --

None.

-- INSTALLATION --

Enable this module.  See further details in the help page.

-- CONFIGURATION --

The field formatter can be configured by managing display settings for a
content type.  See further details in the help page.
